import java.util.Arrays;
import java.util.Random;

public class BusquedaBinariaAleatoria {
    public static int busquedaBinaria(int[] array, int elemento) {
        int izquierda = 0;
        int derecha = array.length - 1;
        int comparaciones = 0;

        while (izquierda <= derecha) {
            int medio = izquierda + (derecha - izquierda) / 2;
            comparaciones++;
            if (array[medio] == elemento) {
                System.out.println("Número de comparaciones realizadas en la búsqueda binaria: " + comparaciones);
                return medio; // Retorna el índice del elemento si se encuentra
            }
            if (array[medio] < elemento) {
                izquierda = medio + 1;
            } else {
                derecha = medio - 1;
            }
        }


        System.out.println("Número de comparaciones realizadas en la búsqueda binaria: " + comparaciones);
        return -1; // Retorna -1 si el elemento no se encuentra en el array
    }

    public static int busquedaLineal(int[] array, int elemento) {

        int comparaciones = 0;
       // Retorna el índice del elemento si se encuentra
       int indiceEnArray = -1;

       for (int i = 0; i < array.length; i++) {
           comparaciones++;
           if (array[i] == elemento) {
               indiceEnArray = i;
               break;
           }
       }

       System.out.println("Número de comparaciones realizadas en la búsqueda lineal: " + comparaciones);

       return indiceEnArray; // Retorna -1 si el elemento no se encuentra en el array
   }

    public static void main(String[] args) {
        int[] array = new int[2000];
        Random random = new Random();

        // Llenamos el array con números aleatorios
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10000); // Genera números aleatorios entre 0 y 9999
        }

        int elementoBuscado = array[random.nextInt(array.length)]; // Se elige un elemento aleatorio del array

        // Búsqueda lineal
        int indice = busquedaLineal(array, elementoBuscado);

        if (indice == -1) {
            System.out.println("El elemento " + elementoBuscado + " no se encuentra en el array.");
        } 

        // Ordenamos el array
        Arrays.sort(array);

        // Elemento a buscar
        

        // Búsqueda binaria
        indice = busquedaBinaria(array, elementoBuscado);

        if (indice != -1) {
            System.out.println("El elemento " + elementoBuscado + " se encuentra en el índice " + indice + ".");
        } else {
            System.out.println("El elemento " + elementoBuscado + " no se encuentra en el array.");
        }
    }


}
