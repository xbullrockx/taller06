import java.util.Random;

public class BusquedaLinealAleatoria {

    public static int busquedaLineal(int[] array, int elemento) {

         // obtenemos el tiempo inicial en milisegundos
        long tiempoInicio = System.currentTimeMillis();

        int comparaciones = 0;
        // Retorna el índice del elemento si se encuentra
        int indiceEnArray = -1;

        for (int i = 0; i < array.length; i++) {
            comparaciones++;
            if (array[i] == elemento) {
                indiceEnArray = i;
                break;
            }
        }

        // obtenemos el tiempo final en milisegundos
        long tiempoFin = System.currentTimeMillis();
    
        // calculamos la diferencia que será el tiempo que tardó la búsqueda lineal
        long tiempoEjecucion = tiempoFin - tiempoInicio;

        System.out.println("Número de comparaciones realizadas en la búsqueda lineal: " + comparaciones);


        // imprimimos el tiempo de ejecución
        System.out.println("Tiempo de ejecución en milisegundos: " + tiempoEjecucion);

        return indiceEnArray; // Retorna -1 si el elemento no se encuentra en el array
    }

    public static void main(String[] args) {
        int[] array = new int[200000];
        Random random = new Random();

        // Llenamos el array con números aleatorios
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(1000000); // Genera números aleatorios entre 0 y 999999
        }

        // Elemento a buscar
        int elementoBuscado = array[random.nextInt(array.length)]; // Se elige un elemento aleatorio del array
        System.out.println("Elemento a buscar: " + elementoBuscado);

        // Búsqueda lineal
        int indice = busquedaLineal(array, elementoBuscado);

        if (indice != -1) {
            System.out.println("El elemento " + elementoBuscado + " se encuentra en el índice " + indice + ".");
        } else {
            System.out.println("El elemento " + elementoBuscado + " no se encuentra en la lista.");
        }
    }
}