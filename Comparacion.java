import java.util.Random;

public class Comparacion {
    
    public static void main(String[] args) {
        int[] array = new int[2000];
        Random random = new Random();

        // Llenamos el array con números aleatorios
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10000); // Genera números aleatorios entre 0 y 9999
        }

        int [] arrayMergeSort = array.clone();
        int [] arrayQuick = array.clone();
        int [] arrayBubble = array.clone();

        long startTime = System.currentTimeMillis();
        mergeSort(arrayMergeSort, arrayMergeSort.length);
        long endTime = System.currentTimeMillis();
        System.out.println("Tiempo de ejecución MergeSort: " + (endTime - startTime) + "ms");
    
        startTime = System.currentTimeMillis();
        QuickSort(arrayQuick,0,array.length - 1);
        endTime = System.currentTimeMillis();
        System.out.println("Tiempo de ejecución QuickSort: " + (endTime - startTime) + "ms");
    
        startTime = System.currentTimeMillis();
        bubbleSort(arrayBubble);
        endTime = System.currentTimeMillis();
        System.out.println("Tiempo de ejecución BubbleSort: " + (endTime - startTime) + "ms");


    }

    public static int bubbleSort(int[] arr) {
        int n = arr.length;
        int temp = 0;
        int count = 0;
        for(int i=0; i < n; i++){
            for(int j=1; j < (n-i); j++){
                count++;
                if(arr[j-1] > arr[j]){
                    temp = arr[j-1];
                    arr[j-1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return count;
    }

     // Función encargada de dividir y ordenar el array
     public static void mergeSort(int[] array, int tamañoArray) {

        // Si el array tiene menos de dos elementos, no necesita ordenarse
        if (tamañoArray < 2)
            return;

        // Se encuentra el punto medio del array
        int puntoMedio = tamañoArray / 2;
        // Se crean dos array para dividirlo en dos
        int[] subarrayIzq = new int[puntoMedio];
        int[] subarrayDer = new int[tamañoArray - puntoMedio];

        // Se llena subarrayIzq con la primera mitad de array
        System.arraycopy(array, 0, subarrayIzq, 0, puntoMedio);
        // Se llena subarrayDer con la segunda mitad de array
        if (tamañoArray - puntoMedio >= 0)
            System.arraycopy(array, puntoMedio, subarrayDer, 0, tamañoArray - puntoMedio);

        // Se ordenan ambos subarrays de forma recursiva
        mergeSort(subarrayIzq, puntoMedio);
        mergeSort(subarrayDer, tamañoArray - puntoMedio);

        // Las dos mitades ordenadas se combinan
        merge(array, subarrayIzq, subarrayDer, puntoMedio, tamañoArray - puntoMedio);
    }

    // Función que combina dos subarrays en un array ordenado
    private static void merge(int[] array, int[] subarrayIzq, int[] subarrayDer,
        
        int tamañoIzq, int tamañoDer) {
        // Índice para subarrayIzq, subarrayDer y array respectivamente
        int indiceIzq = 0, indiceDer = 0, indiceArray = 0;
        
        // Mientras haya elementos sin ver en ambos subarrays
        while (indiceIzq < tamañoIzq && indiceDer < tamañoDer) {
            // Se compara y se añade el menor elemento al array final
            if (subarrayIzq[indiceIzq] <= subarrayDer[indiceDer]) {
                array[indiceArray++] = subarrayIzq[indiceIzq++];
            } else {
                array[indiceArray++] = subarrayDer[indiceDer++];
            }
                        
        }
        
        // Si quedan elementos en alguno de los subarrays, se añaden al array final
        while (indiceIzq < tamañoIzq)
            array[indiceArray++] = subarrayIzq[indiceIzq++];
        while (indiceDer < tamañoDer)
            array[indiceArray++] = subarrayDer[indiceDer++];
    }

    public static void QuickSort(int[] array, int start, int end) {
        if (start < end) {
            // Partition index es el indice donde el array esta particionado en dos partes 
            int partitionIndex = Partition(array, start, end);

            // Llamado recursivo para ordenar las dos partes del array 
            QuickSort(array, start, partitionIndex - 1);
            QuickSort(array, partitionIndex + 1, end);
        }
    }

    private static int Partition(int[] array, int start, int end) {
        // Elegir el ultimo elemento como pivot
        int pivot = array[end];
        // Index de menor elemento
        int i = start - 1;

        for (int j = start; j < end; j++) {
            // Si el elemento actual es menor que el pivot, incrementamos el index del menor elemento
            if (array[j] == pivot) {
                i++;

                // Swap array[i] y array[j]
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }

        // Swap array[i+1] y array[end] o pivot
        int temp = array[i + 1];
        array[i + 1] = array[end];
        array[end] = temp;

        // return el indice de partition
        return i + 1;
    }


}
