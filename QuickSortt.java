import java.util.Arrays;
import java.util.Random;

public class QuickSortt {


    private static int contadorComparaciones = 0;

    public static void QuickSort(int[] array, int start, int end) {
        if (start < end) {
            // Partition index es el indice donde el array esta particionado en dos partes 
            int partitionIndex = Partition(array, start, end);

            // Llamado recursivo para ordenar las dos partes del array 
            QuickSort(array, start, partitionIndex - 1);
            QuickSort(array, partitionIndex + 1, end);
        }
    }

    private static int Partition(int[] array, int start, int end) {
        // Elegir el ultimo elemento como pivot
        int pivot = array[end];
        // Index de menor elemento
        int i = start - 1;

        for (int j = start; j < end; j++) {
            // Si el elemento actual es menor que el pivot, incrementamos el index del menor elemento
            contadorComparaciones++;
            if (array[j] == pivot) {
                i++;

                // Swap array[i] y array[j]
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }

        // Swap array[i+1] y array[end] o pivot
        int temp = array[i + 1];
        array[i + 1] = array[end];
        array[end] = temp;

        // return el indice de partition
        return i + 1;
    }

    
    public static void main(String[] args) {
        int[] array = new int[2000];
        Random random = new Random();

        // Llenamos el array con números aleatorios
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10000); // Genera números aleatorios entre 0 y 9999
        }

        QuickSort(array,0,array.length-1);

        System.out.println("Numero de Comparaciones Registradas: " + contadorComparaciones);
        
    }


}