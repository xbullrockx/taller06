import java.util.Random;

public class BubbleSort {
        public static void main(String[] args) {
        int[] array = new int[2000];
        Random random = new Random();

        // Llenamos el array con números aleatorios
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10000); // Genera números aleatorios entre 0 y 9999
        }

        int contadorComparaciones = bubbleSort(array);
        

        System.out.println("Numero de Comparaciones Registradas: " + contadorComparaciones);

    }

    public static int bubbleSort(int[] arr) {
        int n = arr.length;
        int temp = 0;
        int count = 0;
        for(int i=0; i < n; i++){
            for(int j=1; j < (n-i); j++){
                count++;
                if(arr[j-1] > arr[j]){
                    temp = arr[j-1];
                    arr[j-1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return count;
    }
}
