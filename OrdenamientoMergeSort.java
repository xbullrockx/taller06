import java.util.Arrays;
import java.util.Random;

public class OrdenamientoMergeSort {

    private static int contadorComparaciones = 0;

    public static void main(String[] args) {
        int[] array = new int[2000];
        Random random = new Random();

        // Llenamos el array con números aleatorios
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10000); // Genera números aleatorios entre 0 y 9999
        }

    }

    // Función encargada de dividir y ordenar el array
    public static void mergeSort(int[] array, int tamañoArray) {
        // Si el array tiene menos de dos elementos, no necesita ordenarse
        if (tamañoArray < 2)
            return;

        // Se encuentra el punto medio del array
        int puntoMedio = tamañoArray / 2;
        // Se crean dos array para dividirlo en dos
        int[] subarrayIzq = new int[puntoMedio];
        int[] subarrayDer = new int[tamañoArray - puntoMedio];

        // Se llena subarrayIzq con la primera mitad de array
        System.arraycopy(array, 0, subarrayIzq, 0, puntoMedio);
        // Se llena subarrayDer con la segunda mitad de array
        if (tamañoArray - puntoMedio >= 0)
            System.arraycopy(array, puntoMedio, subarrayDer, 0, tamañoArray - puntoMedio);

        // Se ordenan ambos subarrays de forma recursiva
        mergeSort(subarrayIzq, puntoMedio);
        mergeSort(subarrayDer, tamañoArray - puntoMedio);

        // Las dos mitades ordenadas se combinan
        merge(array, subarrayIzq, subarrayDer, puntoMedio, tamañoArray - puntoMedio);
    }

    // Función que combina dos subarrays en un array ordenado
    private static void merge(int[] array, int[] subarrayIzq, int[] subarrayDer,
        
        int tamañoIzq, int tamañoDer) {
        // Índice para subarrayIzq, subarrayDer y array respectivamente
        int indiceIzq = 0, indiceDer = 0, indiceArray = 0;
        
        // Mientras haya elementos sin ver en ambos subarrays
        while (indiceIzq < tamañoIzq && indiceDer < tamañoDer) {
            // Se compara y se añade el menor elemento al array final
            if (subarrayIzq[indiceIzq] <= subarrayDer[indiceDer]) {
                array[indiceArray++] = subarrayIzq[indiceIzq++];
            } else {
                array[indiceArray++] = subarrayDer[indiceDer++];
            }
            // Incremento el contador de comparaciones por cada comparación realizada
            contadorComparaciones++;
        }
        
        // Si quedan elementos en alguno de los subarrays, se añaden al array final
        while (indiceIzq < tamañoIzq)
            array[indiceArray++] = subarrayIzq[indiceIzq++];
        while (indiceDer < tamañoDer)
            array[indiceArray++] = subarrayDer[indiceDer++];
    }

}