# Taller06



## Parte 1: Busqueda Lineal vs Busqueda Binaria

En esta sección, vamos a realizar una comparación entre los dos algoritmos de búsqueda, Lineal y Binaria.

### Busqueda Lineal

La Búsqueda Lineal consiste en recorrer uno a uno cada elemento de la lista hasta encontrar el elemento que se está buscando. Este algoritmo es bastante costoso en temas de rendimiento, pues va a realizar *n* comparaciones donde *n* es el número de elementos en el arreglo. Va a realizar comparaciones hasta que se encuentre el elemento o se acaben los elementos dentro de la lista, resultando una búsqueda insatisfactoria.

#### Implementacion del algoritmo

Un ejemplo en Java podría ser:

``` java
    //Recibimos el array que vamos a recorrer y el elemento que vamos a buscar.
    public static int busquedaLineal(int[] array, int elemento) {
        // Utilizamos una variable llamada comparaciones para saber cuantas comparaciones se van a realizar buscando el elemento
        int comparaciones = 0;

        //Recorremos el array 
        for (int i = 0; i < array.length; i++) {
            comparaciones++; // sumamos uno por cada iteracion del ciclo for
            
            // comparamos si el elemento en i del array es igual al elemento que buscamos
            if (array[i] == elemento) {
                
                System.out.println("Número de comparaciones realizadas en la búsqueda lineal: " + comparaciones);
                return i; // Retorna el índice del elemento si se encuentra
            }
        }
        
        System.out.println("Número de comparaciones realizadas en la búsqueda lineal: " + comparaciones);
        return -1; // Retorna -1 si el elemento no se encuentra en el array
    }

```

#### Pruebas

Corriendo el Algoritmo en un entorno de Java luego de 10 Iteraciones obtenemos los siguientes resultados

|Iteración| Elemento a buscar | Número de comparaciones | Índice del elemento |
|---|---|---|---|
| 1  |5710|1886|1885|
| 2  |9916|531|530|
| 3  |5508|323|322|
| 4  |9705|516|515|
| 5  |8471|1430|1429|
| 6  |9560|773|772|
| 7  |4127|237|236|
| 8  |4583|1614|1613|
| 9  |5406|718|717|
| 10 |1780|763|762|

En Promedio este algoritmo toma 879.1 iteraciones para una lista de 2000 elementos, esto es aproximadamente el 43% de los *n* elementos de la lista.

Podemos modificar el código para medir el tiempo que toma la busqueda en milisegundos agregando  `System.currentTimeMillis()` al codigo de la siguiente manera:

``` java
    public static int busquedaLineal(int[] array, int elemento) {

         // obtenemos el tiempo inicial en milisegundos
        long tiempoInicio = System.currentTimeMillis();

        ...
        //Codigo Algoritmo

        // obtenemos el tiempo final en milisegundos
        long tiempoFin = System.currentTimeMillis();
    
        // calculamos la diferencia que será el tiempo que tardó la búsqueda lineal
        long tiempoEjecucion = tiempoFin - tiempoInicio;

        System.out.println("Número de comparaciones realizadas en la búsqueda lineal: " + comparaciones);
        // imprimimos el tiempo de ejecución
        System.out.println("Tiempo de ejecución en milisegundos: " + tiempoEjecucion);

        return indiceEnArray; // Retorna -1 si el elemento no se encuentra en el array
    }

    //Adicional tenemos que aumentar el tamaño del array a 200 000 elementos para poder evidenciar el tiempo en ms
    ...
        int[] array = new int[200000];
        Random random = new Random();

        // Llenamos el array con números aleatorios
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(1000000); // Genera números aleatorios entre 0 y 999999
        }
    ...

```

Del cual podemos Obtener los siguientes resultados:

| Elemento a buscar | Número de comparaciones | Tiempo de ejecución (ms) | Índice donde se encuentra |
|:-----------------:|:-----------------------:|:------------------------:|:-------------------------:|
| 45696             | 6661                    | 0                        | 6660                      |
| 668665            | 10148                   | 0                        | 10147                     |
| 245448            | 135576                  | 3                        | 135575                    |
| 80913             | 10331                   | 0                        | 10330                     |
| 318470            | 118398                  | 3                        | 118397                    |
| 296238            | 183772                  | 4                        | 183771                    |
| 681909            | 26127                   | 1                        | 26126                     |
| 201101            | 141010                  | 4                        | 141009                    |
| 200194            | 18311                   | 1                        | 18310                     |
| 314966            | 77700                   | 2                        | 77699                     |
| 836245            | 172515                  | 3                        | 172514                    |
| 143848            | 163985                  | 4                        | 163984                    |
| 459117            | 80808                   | 2                        | 80807                     |
| 374180            | 68102                   | 1                        | 68101                     |
| 111829            | 190342                  | 5                        | 190341                    |

En este escenario *n* es 200 000, el promedio de comparaciones es similar al escenario anterior con aproximadamente 46% de *n* , el promedio en tiempo fue 2ms con un valor maximo de 5 ms con 190342 iteraciones, lo cual nos da como resultado que la cantidad de iteraciones promedio va a ser n/2.

[Código de esta actividad](./BusquedaLinealAleatoria.java) 

### Busqueda Binaria

La busqueda Binaria es un algoritmo de busqueda el cual destaca por su rendimiento donde la cantidad de comparaciones esta dada como log(n)2 + 1 donde *n* es el tamaño del arreglo, por ejemplo si un arreglo contiene 2000 elementos entonces se va a realizar aproximadamente 12 comparaciones, que es mucho mas optimo que una **busqueda lineal** que puede tomar hasta 2000 comparaciones.

Sin Embargo la busqueda binaria requiere que la lista este ordenada, lo cual agrega un requisito al algoritmo.

El algoritmo consiste es "partir" el array a la mitad y comparar ese punto, si en esa comparacion encuentra el elemento el algoritmo finaliza, si no se compara si el elemento a buscar es mayor o menor al elemento de la mitad y dependiendo de ello se vuelve a partir el array hacia el lado correspondiente y recalcula la mitad y vuelve a comparar.

```
Ilustracion del funcionamiento del ALgoritmo

Array Inicial:
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
                  ^
                  '7'

Si 'x' > '7', continuamos con la mitad derecha del array:
[8, 9, 10, 11, 12, 13, 14, 15]
        ^
        '11'

Si 'x' < '11', continuamos con la mitad izquierda del nuevo array:
[8, 9, 10]
   ^
   '9'

Si 'x' > '9', continuamos con la mitad derecha del nuevo array:
[10]
 ^
 '10'
    
El objetivo 'x' se encontró en el índice 9 (iniciando desde 0).

```

#### Implementacion

Para este ejemplo vamos a utilizar Array.Sort, la caul es una funcion de Java que nos permite organizar el array y enfocarno en la implementacion del algoritmo, 

``` java
public static int busquedaBinaria(int[] array, int elemento) {
        //Definimos los limites de nuestro array
        int izquierda = 0;
        int derecha = array.length - 1;
        int comparaciones = 0;

        //Vamos a iterar el arreglo mientras la izquierda sea menor a la derecha
        while (izquierda <= derecha) {
            //calculamos el punto medio
            int medio = izquierda + (derecha - izquierda) / 2;
            comparaciones++; // añadimos una comparacion por iteracion
            if (array[medio] == elemento) {
                System.out.println("Número de comparaciones realizadas en la búsqueda binaria: " + comparaciones);
                return medio; // Retorna el índice del elemento si se encuentra
            }
            //si el elemento buscado no es el elemento en el medio, recalculamos los limites a la izquierda o derecha dependiendo si el elemento fue mayor o menor al medio
            if (array[medio] < elemento) {
                izquierda = medio + 1;
            } else {
                derecha = medio - 1;
            }
        }
        System.out.println("Número de comparaciones realizadas en la búsqueda binaria: " + comparaciones);
        return -1; // Retorna -1 si el elemento no se encuentra en el array
    }

```

Agregamos el Anterior metodo de busqueda lineal antes de organziar el array para poder comarar los resultados, luego de correr ambos algoritmos con un array de 2000 elementos estos fueron los resultados:

| Prueba | Comparaciones B.Lineal | Comparaciones B. Binaria | Elemento | Índice |
|:------:|:---------------------------:|:----------------:|:--------:|:------:|
| 1      | 1137                        | 8                | 1064     | 209    |
| 2      | 935                         | 11               | 2683     | 532    |
| 3      | 1292                        | 10               | 5269     | 1062   |
| 4      | 191                         | 11               | 7592     | 1523   |
| 5      | 1990                        | 11               | 7495     | 1501   |
| 6      | 1942                        | 11               | 5309     | 1097   |
| 7      | 1949                        | 11               | 2426     | 507    |
| 8      | 170                         | 8                | 8097     | 1600   |
| 9      | 1092                        | 9                | 8252     | 1658   |
| 10     | 618                         | 10               | 2916     | 570    |
| 11     | 380                         | 7                | 3135     | 608    |
| 12     | 414                         | 9                | 3487     | 666    |
| 13     | 1448                        | 11               | 5829     | 1206   |
| 14     | 1827                        | 10               | 9266     | 1840   |
| 15     | 176                         | 11               | 467      | 89     |
| 16     | 218                         | 11               | 2989     | 644    |
| 17     | 526                         | 10               | 7308     | 1434   |
| 18     | 729                         | 11               | 9420     | 1894   |
| 19     | 1900                        | 10               | 4125     | 824    |
| 20     | 995                         | 10               | 4891     | 945    |

Para la prueba anterior podemos evidenciar que las comparaciones hechas por la busqueda binaria son mucho menores que la busqueda lineal, en la busqueda lineal hubo un maximo de 11 el cual es aproximado al resultado de log(n)2 + 1 donde n es 2000, mientras que para la busqueda lineal el resultado fue de 1990 comparaciones. 

La desventaja de el algortimo de busqueda binaria es que requiere que la lista ya este ordenada, en esta prueba usamos Array.Sort pero para listas gigantes este ordenamiento puede tardar mas que la busqueda, sin embargo luego de tener ordenada la lista la cantidad de comparaciones es muchisimo menor que una busqueda lineal.

Puede que para casos donde la lista esta desordenada y cambiando constantemente con pocos elementos la busqueda lineal sea mas eficiente, pues se salta el tiempo de ordenamiento, sin embargo en listas que no cambien tanto donde se deba buscar constantemente la busqueda binaria es ideal.


## Parte 2: Comparacion de Algoritmos de Ordenamiento

### Mergsort

Este Algoritmo es basado en la tecnica de "Divide y Venceras" la cual consiste en dividir el array por la mitad y estas mitades en otras de manera recursiva hasta no poder dividir mas y ordenar los arreglos  para finalmente combinar las soluciones., este algoritmo tiene un nivel de complejidad O(n) donde n es el numero de comparaciones en la funcion.

```
Implementacion MergeSort

Vamos a ordenar el siguiente array: [5, 3, 6, 2, 4, 1]

La primera etapa de MergeSort es la división del array en partes más pequeñas.

Array inicial: [5, 3, 6, 2, 4, 1]
Dividimos el array por la mitad:

Primera mitad: [5, 3, 6]
Segunda mitad: [2, 4, 1]
Luego dividimos estas mitades en partes aún más pequeñas:

Primera mitad:
- [5]
- [3, 6]

Segunda mitad:
- [2]
- [4, 1]
Y una vez más dividimos hasta llegar a unidades individuales:

Primera mitad:
- [5]
- [3]
- [6]

Segunda mitad:
- [2]
- [4]
- [1]
Ahora procedemos al algoritmo de "merging" que combina de a dos los sub-arrays más pequeños de manera ordenada:

Primera mitad:
- Merge [5] y [3] --> [3, 5]
- Merge [6] --> [3, 5, 6]

Segunda mitad:
- Merge [2] y [4] --> [2, 4]
- Merge [1] --> [1, 2, 4]
Finalmente, combinamos la primera y segunda mitad:

Merge [3, 5, 6] y [1, 2, 4] --> [1, 2, 3, 4, 5, 6]
```
La implementacion en Codigo de Java la tenemos en el Archivo [OrdenamientoMergeSort.java](./OrdenamientoMergeSort.java)

 ``` java
 // Función encargada de dividir y ordenar el array
    public static void mergeSort(int[] array, int tamañoArray) {

        // Si el array tiene menos de dos elementos, no necesita ordenarse
        if (tamañoArray < 2)
            return;

        // Se encuentra el punto medio del array
        int puntoMedio = tamañoArray / 2;
        // Se crean dos array para dividirlo en dos
        int[] subarrayIzq = new int[puntoMedio];
        int[] subarrayDer = new int[tamañoArray - puntoMedio];

        // Se llena subarrayIzq con la primera mitad de array
        System.arraycopy(array, 0, subarrayIzq, 0, puntoMedio);
        // Se llena subarrayDer con la segunda mitad de array
        if (tamañoArray - puntoMedio >= 0)
            System.arraycopy(array, puntoMedio, subarrayDer, 0, tamañoArray - puntoMedio);

        // Se ordenan ambos subarrays de forma recursiva
        mergeSort(subarrayIzq, puntoMedio);
        mergeSort(subarrayDer, tamañoArray - puntoMedio);

        // Las dos mitades ordenadas se combinan
        merge(array, subarrayIzq, subarrayDer, puntoMedio, tamañoArray - puntoMedio);
    }

    // Función que combina dos subarrays en un array ordenado
    private static void merge(int[] array, int[] subarrayIzq, int[] subarrayDer,
        
        int tamañoIzq, int tamañoDer) {
        // Índice para subarrayIzq, subarrayDer y array respectivamente
        int indiceIzq = 0, indiceDer = 0, indiceArray = 0;
        
        // Mientras haya elementos sin ver en ambos subarrays
        while (indiceIzq < tamañoIzq && indiceDer < tamañoDer) {
            // Se compara y se añade el menor elemento al array final
            if (subarrayIzq[indiceIzq] <= subarrayDer[indiceDer]) {
                array[indiceArray++] = subarrayIzq[indiceIzq++];
            } else {
                array[indiceArray++] = subarrayDer[indiceDer++];
            }
            // Incremento el contador de comparaciones por cada comparación realizada
            contadorComparaciones++;
        }
        
        // Si quedan elementos en alguno de los subarrays, se añaden al array final
        while (indiceIzq < tamañoIzq)
            array[indiceArray++] = subarrayIzq[indiceIzq++];
        while (indiceDer < tamañoDer)
            array[indiceArray++] = subarrayDer[indiceDer++];
    }
```

El anterior algoritmo nos dio los siguientes resultados, donde para un arreglo de 2000 elementos nos arrojo al rededor de 20 mil comparaciones para el metodo de merge.

| Prueba | Comparaciones |
|:------:|:-------------:|
| 1      | 19418         |
| 2      | 19413         |
| 3      | 19417         |
| 4      | 19381         |
| 5      | 19427         |
| 6      | 19424         |
| 7      | 19431         |
| 8      | 19414         |
| 9      | 19433         |
| 10     | 19425         |
| 11     | 19429         |
| 12     | 19418         |
| 13     | 19439         |
| 14     | 19412         |
| 15     | 19399         |
| 16     | 19387         |
| 17     | 19422         |
| 18     | 19465         |
| 19     | 19403         |
| 20     | 19388         |

### QuickSort

Este Algoritmo de ordenamiento consiste en seleccionar un pivote dentro del array, y comenzar a compararlo con los demas elementos de la lista, moviendo los elementos menores a la izquierda y los mayores a la derecha, luego de este ordenamiento el pivote estara en el lugar que le corresponde dentro de la lista, y quedara con dos sub arreglos con los elementos de izquierda y derecha, cada sub arreglo se realiza el mismo proceso de manera recursiva hasta tener el arreglo ordenado.

Este algoritmo tiene dos escenarios de rendimiento dependiendo de donde se escoja el pivote, en el mejor de los casos se escoge un pivote en el centro de la lista y resulta en dos arrays del mismo tamalo los cuales dan una complejidad de O(n·log n).

El Peor de los casos seria que el pivote fuera alguno de los extremos del array y que este ya estuviera ordenado, lo cual haria que se reordenara completamente, esto daria una complejidad de O(n^2).

```
Ilustracion del Funcionamiento del Algoritmo

[9, 4, 5,   2,   6,   7,   1,   8,   3,   0]
                    |
El resultado del primer particionado sería algo como:

[0, 4, 5,   2,   3,   1,   6,   8,   7,   9]
Luego dividimos el array de acuerdo con la nueva posición del pivote y hacemos dos nuevos particionamientos:

Para el subarray izquierdo del pivote [0,4,5,2,3,1], el pivote será 2 (el elemento del centro):

[0,   4,   5,   2,   3,   1]
             |      
El resultado de este particionado sería:

[0, 1, 2, 4, 3, 5]
Para el subarray derecho del pivote [8,7,9], el pivote será 7 (el elemento del centro):

[8,   7,   9]
       |
El resultado de este particionado sería:

[8, 7, 9]
Repetimos esto hasta que todos los subarrays tengan solo un elemento. El array completamente ordenado sería:

[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

Podemos repetir el ejemplo anterior pero escogiendo el pivote en uno de los extremos:

```
Array Inicial: [9, 4, 5, 2, 6, 7, 1, 8, 3, 0]
Pivote: 9 
Luego de la primera iteración, el array se vería de la siguiente manera:

Array: [0, 4, 5, 2, 6, 7, 1, 8, 3, 9]
Nuevo pivote lateral izquierdo: 0
Realizamos el mismo proceso una y otra vez para todas las particiones, terminando con un array que se ve de la siguiente manera:

Array: [0, 4, 5, 2, 6, 7, 1, 8, 3, 9]
Pivote: 0
Array: [0, 1, 5, 2, 6, 7, 4, 8, 3, 9]
Pivote: 1
Array: [0, 1, 2, 5, 6, 7, 4, 8, 3, 9]
Pivote: 2
Array: [0, 1, 2, 3, 6, 7, 4, 8, 5, 9]
Pivote: 3
Y así continuaríamos hasta que el array esté ordenado

Array Final: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```
La implementacion en Codigo de Java la tenemos en el Archivo [QuickSort.java](./QuickSort.java)

``` java
public static void QuickSort(int[] array, int start, int end) {
        if (start < end) {
            // Partition index es el indice donde el array esta particionado en dos partes 
            int partitionIndex = Partition(array, start, end);

            // Llamado recursivo para ordenar las dos partes del array 
            QuickSort(array, start, partitionIndex - 1);
            QuickSort(array, partitionIndex + 1, end);
        }
    }

    private static int Partition(int[] array, int start, int end) {
        // Elegir el ultimo elemento como pivot
        int pivot = array[end];
        // Index de menor elemento
        int i = start - 1;

        for (int j = start; j < end; j++) {
            // Si el elemento actual es menor que el pivot, incrementamos el index del menor elemento
            contadorComparaciones++;
            if (array[j] == pivot) {
                i++;

                // Swap array[i] y array[j]
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }

        // Swap array[i+1] y array[end] o pivot
        int temp = array[i + 1];
        array[i + 1] = array[end];
        array[end] = temp;

        // return el indice de partition
        return i + 1;
    }
```
Despues de ejecutar este codigo durante varias pruebas, obtuimos los siguientes resultados, con un numero mayor de comparaciones, sin embargo este algoritmo es mas eficiente que el mergesort, pero hay que hacer profiling de otra manera:

| Prueba | Comparaciones |
|:------:|:-------------:|
| 1      | 1752169       |
| 2      | 1769104       |
| 3      | 1737979       |
| 4      | 1768577       |
| 5      | 1768116       |
| 6      | 1724475       |
| 7      | 1758127       |
| 8      | 1752653       |
| 9      | 1764177       |
| 10     | 1755249       |
| 11     | 1758294       |
| 12     | 1764316       |
| 13     | 1746879       |
| 14     | 1751542       |
| 15     | 1778398       |
| 16     | 1752181       |

### Ordenamiento Burbuja

Este Algoritmo de ordenamiento es uno de los mas faciles de implementar, el cual consiste en recorrer por pares varias veces el arreglo e ir ordenando los dos elementos del arreglo, este algoritmo se puede iterar multiples veces sobre el arreglo y deja una complejidad de O(n^2)

```
algoritmo de ordenamiento de burbuja:

Paso 1:

[5, 1, 4, 2, 8]

Comparamos los dos primeros elementos y los intercambiamos si el primer número es mayor que el segundo.

[1, 5, 4, 2, 8]

Paso 2:

Ahora, compara los próximos dos elementos y los intercambia si el primer número es mayor que el segundo.

[1, 4, 5, 2, 8]

Paso 3:

Repite el proceso.

[1, 4, 2, 5, 8]

Paso 4:

Continua hasta el fin del array.

[1, 4, 2, 5, 8] - no se ha hecho ningún intercambio aquí.

Paso 5:

En este punto, el número mayor ya está en la posición correcta. Así que, ignoramos este número y repetimos los pasos anteriores para los números restantes.

[1, 2, 4, 5, 8]
```

Implementacion de Codigo java del algoritmo seria:

``` java
    public static int bubbleSort(int[] arr) {
        int n = arr.length;
        int temp = 0;
        int count = 0;
        for(int i=0; i < n; i++){
            for(int j=1; j < (n-i); j++){
                count++;
                if(arr[j-1] > arr[j]){
                    temp = arr[j-1];
                    arr[j-1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return count;
    }
```

Al contar las comparaciones realizadas por este algoritmo nos da un total de 1999000 ya que es la manera en la que recorre todo el arreglo mientras ordena los elementos.

## Comparacion de Rendimiento Y Profiling para los algoritmos de Ordenamiento

Para realizar estas pruebas de comparacion hemos agregado contadores de milisegundos para comparar el rendimiento de los algoritmos para ordenar un array de 2000 elementos:

``` java
 public static void main(String[] args) {
        int[] array = new int[2000];
        Random random = new Random();

        // Llenamos el array con números aleatorios
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10000); // Genera números aleatorios entre 0 y 9999
        }

        int [] arrayMergeSort = array.clone();
        int [] arrayQuick = array.clone();
        int [] arrayBubble = array.clone();

        long startTime = System.currentTimeMillis();
        mergeSort(arrayMergeSort, arrayMergeSort.length);
        long endTime = System.currentTimeMillis();
        System.out.println("Tiempo de ejecución MergeSort: " + (endTime - startTime) + "ms");
    
        startTime = System.currentTimeMillis();
        QuickSort(arrayQuick,0,array.length - 1);
        endTime = System.currentTimeMillis();
        System.out.println("Tiempo de ejecución QuickSort: " + (endTime - startTime) + "ms");
    
        startTime = System.currentTimeMillis();
        bubbleSort(arrayBubble);
        endTime = System.currentTimeMillis();
        System.out.println("Tiempo de ejecución BubbleSort: " + (endTime - startTime) + "ms");
    }
```

Estos fueron los resultados obtenidos:

| Prueba | MergeSort | QuickSort | BubbleSort |
|--------|-----------|-----------|------------|
| 1      | 1ms       | 14ms      | 11ms       |
| 2      | 1ms       | 12ms      | 10ms       |
| 3      | 2ms       | 12ms      | 9ms        |
| 4      | 1ms       | 12ms      | 10ms       |
| 5      | 1ms       | 11ms      | 9ms        |
| 6      | 2ms       | 11ms      | 11ms       |
| 7      | 1ms       | 10ms      | 11ms       |
| 8      | 2ms       | 9ms       | 10ms       |
| 9      | 1ms       | 13ms      | 9ms        |
| 10     | 1ms       | 8ms       | 10ms       |
| 11     | 1ms       | 14ms      | 10ms       |
| 12     | 1ms       | 12ms      | 9ms        |
| 13     | 1ms       | 12ms      | 10ms       |
| 14     | 0ms       | 13ms      | 9ms        |
| 15     | 3ms       | 12ms      | 10ms       |

Para estas rpuebas el algoritmo con mejores resultados fue el MergeSort con una minima de 1ms y una maxima de de 3ms, vs 14ms o 11ms de los otros dos, puede que la implementacion de Quicksort no este del todo correcta pues se supone que deberia tener mayor rendimiento de los otros algoritmos, sin embargo se compara a bubble sort el cual no usamos recursividad en su implementacion.

## Tabla de Complejidad de los Algoritmos

|  Algoritmo | Mejor caso | Caso promedio |  Peor caso |
|:----------:|:----------:|:-------------:|:----------:|
| MergeSort  | O(n log n) | O(n log n)    | O(n log n) |
| QuickSort  | O(n log n) | O(n log n)    | O(n^2)     |
| BubbleSort | O(n)       | O(n^2)        | O(n^2)     |

O(n log n): Este es el tiempo de ejecución para los mejores, promedios y peores casos de MergeSort y para el mejor y caso promedio de QuickSort. Esto significa que el tiempo de ejecución aumenta logarítmicamente para cada entrada adicional en el conjunto de datos. Son más eficientes que los algoritmos con tiempo O(n^2).

O(n^2): Este es el tiempo de ejecución para los casos promedios y peores de BubbleSort y para el peor caso de QuickSort. Esto significa que el tiempo de ejecución aumenta cuadráticamente con cada entrada adicional en el juego de datos. Por lo tanto, para un conjunto de datos grande, estos algoritmos no son tan eficientes.

O(n): Este es el tiempo de ejecución para el mejor caso de BubbleSort. Esto significa que el tiempo de ejecución aumenta linealmente con cada entrada adicional en el juego de datos. Este es el caso más eficiente. Sin embargo, en BubbleSort, este caso ocurre sólo cuando el conjunto de datos ya está ordenado, lo cual no es común en la mayoría de los casos prácticos.
